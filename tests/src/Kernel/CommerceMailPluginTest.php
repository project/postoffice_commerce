<?php

namespace Drupal\Tests\postoffice_commerce\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Tests\postoffice_compat\Kernel\CompatTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\commerce_order\Comparator\AdjustmentComparator;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_price\Comparator\NumberComparator;
use Drupal\commerce_price\Comparator\PriceComparator;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_store\StoreCreationTrait;
use Drupal\profile\Entity\Profile;
use SebastianBergmann\Comparator\Factory as PhpUnitComparatorFactory;

/**
 * Tests the sending of order receipt emails.
 *
 * @group postoffice_commerce
 */
class CommerceMailPluginTest extends CompatTestBase {

  use StoreCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    /* see EntityKernelTestBase */
    'user',
    'system',
    'field',
    'text',
    'filter',

    /* see CommerceKernelTestBase */
    'address',
    'datetime',
    'entity',
    'options',
    'inline_entity_form',
    'views',
    'commerce',
    'commerce_price',
    'commerce_store',
    'path',
    'path_alias',

    /* see OrderKernelTestBase */
    'entity_reference_revisions',
    'profile',
    'state_machine',
    'commerce_number_pattern',
    'commerce_product',
    'commerce_order',

    /* system under test */
    'postoffice_commerce',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.date')
      ->set('country.default', 'US')
      ->save();

    /* see EntityKernelTestBase */
    $this->installEntitySchema('user');
    $this->installConfig(['field']);

    $factory = PhpUnitComparatorFactory::getInstance();
    $factory->register(new NumberComparator());
    $factory->register(new PriceComparator());

    $this->installEntitySchema('path_alias');
    $this->installEntitySchema('commerce_currency');
    $this->installEntitySchema('commerce_store');
    $this->installConfig(['commerce_store']);

    $currency_importer = $this->container->get('commerce_price.currency_importer');
    $currency_importer->import('USD');

    /* see OrderKernelTestBase */
    PhpUnitComparatorFactory::getInstance()->register(new AdjustmentComparator());

    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_variation');
    $this->installConfig(['commerce_product', 'commerce_order']);
    $this->installSchema('commerce_number_pattern', ['commerce_number_pattern_sequence']);

    $this->config('system.mail')
      ->set('interface.commerce', 'postoffice_commerce_mail')
      ->save();
  }

  /**
   * Tests sending a basic email, without any custom parameters.
   */
  public function testBasicEmail() {
    $mailHandler = $this->container->get('commerce.mail_handler');

    $body = [
      '#markup' => '<p>Mail Handler Test</p>',
    ];
    $recordedEmails = $this->callAndRecordEmails(function () use ($body, $mailHandler) {
      $mailHandler->sendMail('customer@example.com', 'Test subject', $body);
    });

    $this->assertCount(1, $recordedEmails);
    /** @var \Drupal\postoffice_commerce\Email\CommerceEmail $email */
    $email = end($recordedEmails);
    $this->assertEquals('customer@example.com', $email->getTo()[0]->toString());
    $this->assertEquals([], $email->getCc());
    $this->assertEquals([], $email->getBcc());
    $this->assertEquals([], $email->getReplyTo());
    $this->assertEquals('Test subject', $email->getSubject());
    $this->assertStringContainsString('Mail Handler Test', $email->getHtmlBody());
    $this->assertEquals('en', $email->getLangcode());
  }

  /**
   * Verify that the order receipt is delivered using the CommerceMail plugin.
   */
  public function testSendOrderReceipt() {
    /* see CommerceKernelTestBase::setUp() */
    $store = $this->createStore('Default store', 'admin@example.com');

    /* see OrderReceiptMailTest::setUp() */

    // An order item type that doesn't need a purchasable entity.
    OrderItemType::create([
      'id' => 'test',
      'label' => 'Test',
      'orderType' => 'default',
    ])->save();

    $user = $this->createUser([], NULL, FALSE, [
      'mail' => 'customer@example.com',
      'preferred_langcode' => 'en',
    ]);

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $product = Product::create([
      'type' => 'default',
      'title' => 'Default testing product',
    ]);
    $product->save();

    $variation1 = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => new Price('12.00', 'USD'),
    ]);
    $variation1->save();
    $product->addVariation($variation1)->save();

    $profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'US',
        'postal_code' => '53177',
        'locality' => 'Milwaukee',
        'address_line1' => 'Pabst Blue Ribbon Dr',
        'administrative_area' => 'WI',
        'given_name' => 'Frederick',
        'family_name' => 'Pabst',
      ],
    ]);
    $profile->save();
    $profile = $this->reloadEntity($profile);

    /** @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage */
    $order_item_storage = $this->container->get('entity_type.manager')->getStorage('commerce_order_item');
    $order_item1 = $order_item_storage->createFromPurchasableEntity($variation1);
    $order_item1->save();
    $order = Order::create([
      'type' => 'default',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'ip_address' => '127.0.0.1',
      'order_number' => '2017/01',
      'billing_profile' => $profile,
      'store_id' => $store->id(),
      'order_items' => [$order_item1],
      'state' => 'completed',
    ]);
    $order->save();
    $order = $this->reloadEntity($order);

    /** @var \Drupal\commerce_order\Mail\OrderReceiptMailInterface orderReceiptMail */
    $orderReceiptMail = $this->container->get('commerce_order.order_receipt_mail');

    $recordedEmails = $this->callAndRecordEmails(function () use ($order, $orderReceiptMail) {
      $orderReceiptMail->send($order);
    });

    $this->assertCount(1, $recordedEmails);
    /** @var \Drupal\postoffice_commerce\Email\CommerceEmail $email */
    $email = end($recordedEmails);
    $this->assertEquals('"Default store" <admin@example.com>', $email->getFrom()[0]->toString());
    $this->assertEquals('customer@example.com', $email->getTo()[0]->toString());
    $this->assertEquals([], $email->getBcc());
    $this->assertEquals('Order #2017/01 confirmed', $email->getSubject());
    $this->assertStringContainsString('Thank you for your order!', $email->getHtmlBody());
    $this->assertStringContainsString('Pabst Blue Ribbon Dr', $email->getHtmlBody());
    $this->assertEquals('en', $email->getLangcode());

    $recordedEmails = $this->callAndRecordEmails(function () use ($order, $orderReceiptMail) {
      $orderReceiptMail->send($order, 'custom@example.com', 'store@example.com');
    });

    /** @var \Drupal\postoffice_commerce\Email\CommerceEmail $email */
    $this->assertCount(1, $recordedEmails);
    $email = end($recordedEmails);
    $this->assertEquals('"Default store" <admin@example.com>', $email->getFrom()[0]->toString());
    $this->assertEquals('custom@example.com', $email->getTo()[0]->toString());
    $this->assertEquals('store@example.com', $email->getBcc()[0]->toString());
    $this->assertEquals('Order #2017/01 confirmed', $email->getSubject());
    $this->assertStringContainsString('Thank you for your order!', $email->getHtmlBody());
    $this->assertStringContainsString('Pabst Blue Ribbon Dr', $email->getHtmlBody());
    $this->assertEquals('en', $email->getLangcode());
  }

  /**
   * Reloads the given entity from the storage and returns it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be reloaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The reloaded entity.
   */
  protected function reloadEntity(EntityInterface $entity) {
    $entityTypeManager = $this->container->get('entity_type.manager');
    $controller = $entityTypeManager->getStorage($entity->getEntityTypeId());
    $controller->resetCache([$entity->id()]);
    return $controller->load($entity->id());
  }

}
