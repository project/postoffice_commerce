<?php

namespace Drupal\postoffice_commerce\Plugin\Mail;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\postoffice_commerce\Email\CommerceEmail;
use Drupal\postoffice_commerce\Email\OrderReceiptEmail;
use Drupal\postoffice_compat\Plugin\Mail\CompatMailBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\RawMessage;

/**
 * Defines a mail backend for Commerce, using Symfony Mailer via Postoffice.
 *
 * @Mail(
 *   id = "postoffice_commerce_mail",
 *   label = @Translation("Postoffice Commerce Mail"),
 *   description = @Translation("Sends message from the core user module, using Symfony Mailer via Postoffice.")
 * )
 */
class CommerceMail extends CompatMailBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('postoffice.mailer'),
      $container->get('logger.channel.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function emailFromMessage(array $message): RawMessage {
    switch ($message['id']) {
      case 'commerce_order_receipt':
        return OrderReceiptEmail::createFromMessage($message);

      default:
        return CommerceEmail::createFromMessage($message);
    }
  }

}
