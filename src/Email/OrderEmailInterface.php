<?php

namespace Drupal\postoffice_commerce\Email;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Interface for commerce email about a specific order.
 */
interface OrderEmailInterface {

  /**
   * Returns the commerce order.
   *
   * Accessible via email.order from twig templates.
   */
  public function getOrder(): OrderInterface;

}
