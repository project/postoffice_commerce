Contents
--------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * See Also


Introduction
------------

The [Postoffice](https://www.drupal.org/project/postoffice) module sends themed
emails with [Symfony Mailer](https://symfony.com/doc/current/mailer.html).

This module provides a mail plugin for commerce compatibility.

 * For a full description of the module, visit the
   [project page](https://www.drupal.org/project/postoffice_commerce)
 * To submit bug reports and feature suggestions, or track changes, visit the
   [issue tracker](https://www.drupal.org/project/issues/postoffice_commerce)


Requirements
------------

This module requires [Postoffice](https://www.drupal.org/project/postoffice) and
[Drupal Commerce](https://www.drupal.org/project/commerce)


Installation
------------

 * Install as you would normally install a contributed Drupal module. Visit the
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
   documentation page for further information.


Configuration
-------------

 * Setup `postoffice_commerce_mail` as the mail plugin for mails sent by commerce:
   ```
   drush config:set system.mail interface.commerce postoffice_commerce_mail
   ```
   Note: Many commerce extensions (such as `commerce_shipping`) use the commerce
   core mail handler to send mails. Those are covered with the configuration
   above.
   One exception is `commerce_license`. This project relies on the core mail
   manager. For this reason it is necessary to also configure the Postoffice
   Commerce Mail plugin for `commerce_license` explicitly:
   ```
   drush config:set system.mail interface.commerce_license postoffice_commerce_mail
   ```


See Also
--------

This module is geared towards developers. If it does not suit your modus
operandi, please have a look at the
[Symfony Mailer](https://www.drupal.org/project/symfony_mailer) module.


Maintainers
-----------

Current maintainers:

 * [znerol](https://www.drupal.org/u/znerol)
